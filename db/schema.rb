# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170202115129) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "collaborators", force: :cascade do |t|
    t.integer  "hospital_id"
    t.integer  "personnel_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["hospital_id"], name: "index_collaborators_on_hospital_id", using: :btree
    t.index ["personnel_id"], name: "index_collaborators_on_personnel_id", using: :btree
  end

  create_table "hospitals", force: :cascade do |t|
    t.integer  "number",     null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["number"], name: "index_hospitals_on_number", using: :btree
  end

  create_table "personnels", force: :cascade do |t|
    t.string   "surname",       limit: 30, null: false
    t.integer  "year_of_birth",            null: false
    t.integer  "position",                 null: false
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["position"], name: "index_personnels_on_position", using: :btree
    t.index ["surname"], name: "index_personnels_on_surname", using: :btree
  end

end
