class CreatePersonnels < ActiveRecord::Migration[5.0]
  def change
    create_table :personnels do |t|
      t.string :surname, null: false, limit: 30, index: true
      t.integer :year_of_birth, null: false, limit: 4
      t.integer :position, null: false, index: true
      t.timestamps
    end
  end
end
