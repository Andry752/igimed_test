class CreateHospitals < ActiveRecord::Migration[5.0]
  def change
    create_table :hospitals do |t|
      t.integer :number, null: false, index: true
      t.timestamps
    end
  end
end
