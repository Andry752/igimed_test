class CreateCollaborators < ActiveRecord::Migration[5.0]
  def change
    create_table :collaborators do |t|
      t.belongs_to :hospital, index: true
      t.belongs_to :personnel, index: true
      t.timestamps
    end
  end
end
