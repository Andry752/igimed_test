class CooperationController < ApplicationController
  def create
    hire_personnel ? head(:ok) : head(:bad_request)
  end

  def destroy
    fire_personnel ? head(:ok) : head(:bad_request)
  end

  private

  def hire_personnel
    hospital&.personnels << personnel
  end

  def fire_personnel
    Collaborator.find(collabarator_params[:id]).destroy
  end

  def hospital
    Hospital.find(hospital_params[:hospital_id])
  end

  def personnel
    Personnel.find(personnel_params[:personnel_id])
  end

  def hospital_params
    params.permit(:hospital_id)
  end

  def personnel_params
    params.permit(:personnel_id)
  end

  def collabarator_params
    params.permit(:id)
  end
end
