class PersonnelsController < ApplicationController
  def show
    @personnel = Personnel.find(personnel_id)
  end

  def index
    @personnels = Personnel.all
  end

  def create
    save_new_personnel ? head(:ok) : head(:bad_request)
  end

  def destroy
    delete_personnel ? head(:ok) : head(:bad_request)
  end

  def update
    personnel&.update(personnel_update_params)
    redirect_to personnel_path(personnel_id)
  end

  private

  def personnel
    Personnel.find(personnel_id)
  end

  def save_new_personnel
    Personnel.new(personnel_params).save
  end

  def delete_personnel
    personnel&.destroy
  end

  def personnel_params
    params.permit(:surname, :year_of_birth, :position)
  end

  def personnel_update_params
    params.require(:personnel).permit(:surname, :year_of_birth, :position)
  end

  def personnel_id
    params[:id]
  end
end
