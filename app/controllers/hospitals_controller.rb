class HospitalsController < ApplicationController
  def show
    @hospital = Hospital.find(hospital_id)
    @avaible_personnels = Personnel.where.not(id: @hospital.personnels.select(:id))
  end

  def index
    @hospitals = Hospital.all
  end

  def create
    save_new_hospital ? head(:ok) : head(:bad_request)
  end

  def destroy
    delete_hospital ? head(:ok) : head(:bad_request)
  end

  private

  def save_new_hospital
    Hospital.new(hospital_params).save
  end

  def delete_hospital
    Hospital.find(hospital_id)&.destroy
  end

  def hospital_params
    params.permit(:number)
  end

  def hospital_id
    params[:id]
  end
end
