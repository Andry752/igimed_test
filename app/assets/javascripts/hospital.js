$( document ).on('turbolinks:load', function() {
  $("#submit_modal").on('click',function () {
    var inputValue = parseInt(document.getElementById("modal_hospital_number").value);
    if (typeof parseInt(inputValue) === 'number')
    {
      $.ajax({
        url: '/hospitals',
        method: 'POST',
        data: {
          number: inputValue
        },
        success: function(data) {
          addNewRow(inputValue);
          $('#new_hospital_modal').modal('hide');
        },
        error: function(error) {
          console.log(error);
        }
      });
    }
    else
    {
      $('#modal_error').removeClass("hidden");
    }
  });

  $(document).on('click', '#delete_hospital', function(e) {
    e.preventDefault();
    if (confirm("Are you sure?"))
    {
      var hospitalID = $(e.target).attr('hostital_id')
      $.ajax({
        url: '/hospitals/' + hospitalID,
        method: 'DELETE',
        data: {},
        success: function(data) {
          $(e.target).parent().parent().hide();
        },
        error: function(error) {
          console.log(error);
        }
      });
    }
  });

  function addNewRow(inputValue) {
    lastRow = $("#hospitals_table tr:last");
    lastIndex = parseInt($("#hospitals_table tr:last td:first").text());
    lastId = parseInt(lastRow.attr('id'));
    delet_link = 'Delete'
    hospital_link = "<a href='/hospitals/"+ (lastId + 1) +"'>"+ inputValue +"</a>"
    lastRow.after('<tr><td>'+ (lastIndex + 1) +'</td><td>'+ hospital_link +'</td><td>'+ delet_link +'</td></tr>');
  }

  $(document).on('click', '#hire_personnel', function(e){
    e.preventDefault();
    var personnelID = $(e.target).attr('personnel_id');
    var hospitalId = $('h3').attr('id');
    $.ajax({
        url: '/cooperation',
        method: 'POST',
        data: {
          hospital_id: hospitalId,
          personnel_id: personnelID
        },
        success: function(data) {
          $(e.target).parent().parent().hide();
        },
        error: function(error) {
          console.log(error);
        }
      });
  });

  $(document).on('click', '#fire_personnel', function(e){
    e.preventDefault();
    var collabaratorID = $(e.target).attr('colloborator_id');
    console.log(e);
    $.ajax({
        url: '/cooperation/'+ collabaratorID,
        method: 'DELETE',
        data: {
        },
        success: function(data) {
         $(e.target).parent().parent().hide();
        },
        error: function(error) {
          console.log(error);
        }
      });
  });
});
