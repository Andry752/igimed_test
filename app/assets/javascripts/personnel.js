$( document ).on('turbolinks:load', function() {
  $("#submit_modal_personnel").on('click',function () {
    var surname =$('#modal_personnel_surname').val();
    var year_of_birth = $('#modal_personnel_year_of_birth').val();
    var position = $('#modal_personnel_position').val();
    if (typeof parseInt(year_of_birth) === 'number' && year_of_birth > 1900 && year_of_birth < new Date().getFullYear())
    {
      $.ajax({
        url: '/personnels',
        method: 'POST',
        data: {
          surname: surname,
          year_of_birth: year_of_birth,
          position: position
        },
        success: function(data) {
          addNewRow(surname, year_of_birth, position);
          $('#new_personnel_modal').modal('hide');
        },
        error: function(error) {
          console.log(error);
        }
      });
    }
    else
    {
      $('#modal_error_year').removeClass("hidden");
    }
  });

  $(document).on('click', '#delete_personnel', function(e) {
    e.preventDefault();
    if (confirm("Are you sure?"))
    {
      var personnelID = $(e.target).attr('personnel_id')
      $.ajax({
        url: '/personnels/' + personnelID,
        method: 'DELETE',
        data: {},
        success: function(data) {
          $(e.target).parent().parent().hide();
        },
        error: function(error) {
          console.log(error);
        }
      });
    }
  });

  function addNewRow(surname, year_of_birth, position) {
    lastRow = $("#personnel_table tr:last");
    lastIndex = parseInt($("#personnel_table tr:last td:first").text());
    lastId = parseInt(lastRow.attr('id'));
    delet_link = "<a id='delete_personnel' personnel_id='1' href='#'>Delete</a>"
    personnel_link = "<a href='/personnel/"+ (lastId + 1) +"'>"+ surname +"</a>"
    lastRow.after('<tr><td>'+ (lastIndex + 1) +'</td><td>'+ personnel_link +'</td><td>'+ year_of_birth +'</td><td>'+ position +'</td><td>'+ delet_link +'</td></tr>');
  }
});
