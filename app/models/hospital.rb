class Hospital < ApplicationRecord
  has_many :collaborators, dependent: :destroy
  has_many :personnels, through: :collaborators

  validates :number,
            presence: true,
            uniqueness: true
end
