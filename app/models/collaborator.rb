class Collaborator < ApplicationRecord
  belongs_to :hospital
  belongs_to :personnel

  validates :hospital_id,
            uniqueness: { scope: :personnel_id }
end
