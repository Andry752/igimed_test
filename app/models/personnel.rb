class Personnel < ApplicationRecord
  has_many :collaborators, dependent: :destroy
  has_many :hospitals, through: :collaborators

  validates :surname,
            presence: true,
            length: { maximum: 30 }

  validates :year_of_birth,
            presence: true,
            numericality: { only_integer: true,
                            greater_than_or_equal_to: 1900,
                            less_than_or_equal_to: ->(_brewery){ Date.current.year } }

  validates :position,
            presence: true

  enum position: %i(doctor nurse paramedic)
end
