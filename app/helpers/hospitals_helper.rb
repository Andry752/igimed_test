module HospitalsHelper
  def collabarator_id(hospital_id, personnel_id)
    Hospital.find(hospital_id).collaborators.where(personnel_id: personnel_id).first.id
  end
end
