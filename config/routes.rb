Rails.application.routes.draw do
  root 'hospitals#index'
  resources :hospitals
  resources :personnels
  resources :cooperation
end
